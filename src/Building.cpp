#include "./Building.hpp"

Building::Building(std::istream& input) {
    input >> this->_width >> this->_height;
}

int Building::getWidth() {
    return this->_width;
}

int Building::getHeight() {
    return this->_height;
}
