#ifndef BOMB_H_
#define BOMB_H_

#include <fstream>

class Bomb {
    private:
        int _timer;
    public:
        Bomb(std::istream& input);
        int tick();
};

#endif // BOMB_H_
