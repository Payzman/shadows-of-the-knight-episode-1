#ifndef BUILDING_H_
#define BUILDING_H_

#include <fstream>

class Building {
    private:
        int _width;
        int _height;
    public:
        Building(std::istream& input);
        int getWidth();
        int getHeight();
};

#endif // BUILDING_H_
