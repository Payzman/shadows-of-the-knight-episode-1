#ifndef BATMAN_H_
#define BATMAN_H_

#include <fstream>

class Batman {
    private:
        int _x;
        int _y;
    public:
        Batman(std::istream&);
        int getX();
        int getY();

};

#endif // BATMAN_H_
