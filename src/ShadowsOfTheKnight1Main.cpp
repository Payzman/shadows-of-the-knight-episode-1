#include "./ShadowsOfTheKnight1Main.hpp"

int main()
{
    Building building(std::cin); // first input: Building dimensions
    Bomb bomb(std::cin); // second input: Max. Number of turns
    int X0;
    int Y0;
    std::cin >> X0 >> Y0; std::cin.ignore();
    // game loop
    do {
        std::string bombDir;    // the direction of the bombs from batman's
                                // current location (U, UR, R, DR, D, DL, L or
                                //  UL)
        std::cin >> bombDir; std::cin.ignore();

        // the location of the next window Batman should jump to.
        std::cout << "0 0" << std::endl;
    }  while (bomb.tick()); // bomb should tick only after each round
}
