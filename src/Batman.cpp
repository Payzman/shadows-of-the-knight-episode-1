#include "./Batman.hpp"

Batman::Batman(std::istream& input) {
    input >> this->_x >> this->_y;
}

int Batman::getX() {
    return this->_x;
}

int Batman::getY() {
    return this->_y;
}
