#include "./Bomb.hpp"

Bomb::Bomb(std::istream& input) {
    input >> this->_timer;
}

int Bomb::tick() {
    this->_timer--;
    return this->_timer;
}
