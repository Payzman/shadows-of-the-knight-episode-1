#include "./ShadowsOfTheKnight1Test.hpp"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"

/* Create Building with defined Width and Height (Building class)
 */
TEST(Building, Create) {
    std::ifstream input;
    input.open("./test/building.mock", std::ifstream::in);     // 4 5
    Building building(input);
    input.close();
    ASSERT_EQ(building.getWidth(), 4);
    ASSERT_EQ(building.getHeight(), 5);
}

/* Create bomb with defined timer. The function bomb.tick() subtracts 1 from
 * the internal timer.
 */
TEST(Bomb, Create) {
    std::ifstream input;
    input.open("./test/bomb.mock", std::ifstream::in);     // 10
    Bomb bomb(input);
    input.close();
    ASSERT_EQ(bomb.tick(), 9);
    ASSERT_EQ(bomb.tick(), 8);
}

/* Create batman with defined starting coordinates
 */
TEST(Bomb, Batman) {
    std::ifstream input;
    input.open("./test/batman.mock", std::ifstream::in);     // 3 4
    Batman batman(input);
    input.close();
    ASSERT_EQ(batman.getX(), 3);
    ASSERT_EQ(batman.getY(), 4);
}

/* Define a dangerzone for the building consisting of minimum and maximum x and
 * y values. Starting values are building.width and building.height
 */

/* Batman has a heat-seeking device. He can use it via batman.searchBomb() to
 * see in which direction the bomb is. This updates the building.dangerzone
 */

/* Batman can jump to the next windows via batman.jump() - to do a binary
 * search he is going to jump right in the middle of the building.dangerzone
 */

#pragma clang diagnostic pop
